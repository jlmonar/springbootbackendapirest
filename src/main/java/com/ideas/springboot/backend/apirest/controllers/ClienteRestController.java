package com.ideas.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ideas.springboot.backend.apirest.models.entity.Cliente;
import com.ideas.springboot.backend.apirest.models.services.IClienteService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class ClienteRestController {
	@Autowired
	IClienteService clienteService;

	@GetMapping("/clientes")
	public List<Cliente> index() {
		return clienteService.findAll();
	}

	@GetMapping("/clientes/{id}")
	public Cliente finById(@PathVariable Long id) {
		return clienteService.findById(id);
	}

	/*
	 * Recibimos el objeto Cliente que contiene los datos para que se persistan,
	 * estos son los datos en formato json que está enviando nuestro cliente con
	 * angular, pero acá se va a transformar en un objeto Cliente, y esto es lo que
	 * se va a persistir, y como viene en formato Json dentro del cuerpo de la
	 * petición del request, entonces debemos indicar que es un @RequestBody para
	 * poder transformar esos datos, para que Spring los vaya a buscar, tome esos
	 * parámetros y los pueble o los mapee al objeto cliente y lo guardamos.
	 */
	/*
	 * La anotación @ResponseStatus sirve para indicar el código de respuesta en
	 * caso de que la petición resulta exitosa. Si no se usa esta anotación, por
	 * defecto se retorna el código 200 de ('OK').
	 */
	@PostMapping("/clientes")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente save(@RequestBody Cliente cliente) {
		return clienteService.save(cliente);
	}

	@PutMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente update(@RequestBody Cliente cliente, @PathVariable Long id) {
		Cliente clienteActual = clienteService.findById(id);

		clienteActual.setNombre(cliente.getNombre());
		clienteActual.setApellido(cliente.getApellido());
		clienteActual.setEmail(cliente.getEmail());

		return clienteService.save(clienteActual);
	}

	@DeleteMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		clienteService.delete(id);
	}
}
